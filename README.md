## Apache endpoint for FTS3 in the HIFIS Transfer Service 
This repo features the implementation and configuration details for an Apache2-server that is capable of being addressed by CERN's FTS3 as a passive endpoint in order to do 3rd-party-copy (TPC) of large datasets. The Apache2 instance has been chosen because of its ubiquity and reliability. The modules that are used are tested and externally maintained, apart from a patch applied to the optional mpm-itk.

Here, we provide a Docker image of an FTS3-compatible Apache server that can be deployed on kubernetes by means of the helm charts in the subdirectory `hts-apache`. 
On the branch `local_testing`, there is also a standalone version of the docker image together with the appropriate environment and instructions.

### Purpose and features of this endpoint realisation in short:
- Apache2 webserver capable of serving files through utilisation of the WebDAV protocol.
- WebDAV endpoint is secured by OAuth2
- With the mpm-itk module, it is possible to map a remote user to a local user on the system and read & write files to the filesystem while honouring the local user's ACLs
- Together with a prototype module the Apache is also able to compare checksums of transmitted files in accordance with RFC 3230.
- Checksums for files are also calculated when they are transferred via the file system, i.e. not through the Apache, by a wrapper for `inotifywatch`.
- Easily deployable on a Kubernetes cluster by means of the provided helm charts.
- Deployable as a standalone docker container as well.

### Prerequisites:
- a Kubernetes cluster with
    - an ingress controller with HTTPS access and a URL for the deployment
    - a cert manager
- a WLCG grid host certificate for HTTPS connections to other endpoints
- an OIDC client registered with the Helmholtz AAI

### Installation:
Build the Docker image with the provided Dockerfile either locally or get access to the gitlab-registry at the HZDR-gitlab
The endpoint is deployable with the helm-charts in the subdirectory `hts-apache`.
Clone the repository to a machine with `helm` access to your Kubernetes cluster, modify the `values.yaml` and execute `helm install -n <name space> <deployment name> ./hts-apache`.
In the `values.yaml` you need to modify the following values:
- `image/registry` and `image/repository` to point to the docker registry where you image is located
- `imagePullSecrets/name` for the k8s-secret with the credentials to the registry
- `environmenVariables` 
    - `WEBMASTER_MAIL` with your email address
    - `YOUR_SERVER_NAME` with your hostname/URL
    - `ALLOWED_IP_RANGE` for the readiness and liveness probes in your kubernetes cluster
    - `YOUR_REDIRECT_URI` with the registered redirect_uri of your OIDC client
    - ... the other environment variables could be changed but work as they are given
- `volumes/dataVolume` with the name of the persistent volume claim for storing the files to be transferred (mounted to /mnt/data in the Docker container)
- `volumes/appVolume` with the name of the persistent volume claim for storing the runtime data of the apache (mounted to /var/run/apache2 in the Docker container)
- `oidcSecrets/secretName` with a k8s-secret that holds a key-value pair of `client_id:client_secret` for you OIDC client
- `ingress`
    - `annotations/cert-manager.io/cluster-issuer` with your cluster issuer for generating a certificate if you want to, otherwise use the WLCG grid host cert
    - `hosts/host` with your endpoint's URL
    - `tls/secretName` with your TLS secret
    - `tls/hosts` with your endpoint's URL

### Nota bene:
The endpoint is configured to precalculate and cache checksums for data integrity checks with `mod_want_digest` [Repo](github.com/wetzel-desy/mod_want_digest).
While the Apache webserver takes care of caching checksums for files transferred via HTTP, files copied onto the file system by other means are taken care of differently.
In the docker container, dir_watcher.py is used with an inotifywatch-daemon to track all files in the directory served by the endpoint that are manipulated by local file system commands and create/modify/move/delete checksums for mod_want_digest.
The cached checksums are stored in the volume `appData` attached to the Docker container.

### Support
If you have any questions, need support for setting up the endpoint or have suggestions for new features, please do not hesitate to contact us: [Support](https://www.hifis.net/contact.html).
