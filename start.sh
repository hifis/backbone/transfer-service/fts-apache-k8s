#!/bin/bash

# using environment variables from helm chart to define locations

# check for environment variables and replace corresponding values in openidc.conf and activate config
# values: YOUR_LOCATION_FOR_APACHE2_VAR YOUR_MANDATORY_CRYPTO_PHRASE YOUR_CLIENT_ID YOUR_CLIENT_SECRET

#apache2ctl -M
apache2ctl stop

if [[ -z "$YOUR_LOCATION_FOR_APACHE2_VAR" || -z "$YOUR_CLIENT_ID" || -z "$YOUR_CLIENT_SECRET" || -z "$YOUR_REDIRECT_URI" ]]
then 
    echo "One or more variables needed for openidc.conf have not been set. Please check your config for YOUR_LOCATION_FOR_APACHE2_VAR, YOUR_CLIENT_ID and YOUR_CLIENT_SECRET. Exiting..."
    exit 1
fi
# setting the cryptophrase for openidc.conf to a random 64 character string based on the current date in seconds since 1.1.1970, hash with SHA256, encode in base64 and take the first 64 chars.
YOUR_MANDATORY_CRYPTO_PHRASE=$(date +%s | sha256sum | base64 | head -c 64)

# note the usage of # as a delimiter for sed here because the standard '/' would clash with slashes in the paths set by the environment variables
sed -i "s#YOUR_LOCATION_FOR_APACHE2_VAR#$YOUR_LOCATION_FOR_APACHE2_VAR#g" /etc/apache2/conf-available/openidc.conf
sed -i "s#YOUR_MANDATORY_CRYPTO_PHRASE#$YOUR_MANDATORY_CRYPTO_PHRASE#g" /etc/apache2/conf-available/openidc.conf
sed -i "s#YOUR_CLIENT_ID#$YOUR_CLIENT_ID#g" /etc/apache2/conf-available/openidc.conf
sed -i "s#YOUR_CLIENT_SECRET#$YOUR_CLIENT_SECRET#g" /etc/apache2/conf-available/openidc.conf
sed -i "s#YOUR_REDIRECT_URI#$YOUR_REDIRECT_URI#g" /etc/apache2/conf-available/openidc.conf

# replace values in default-ssl.conf and activate
# values: LOCATION_OF_YOUR_CERTIFICATE LOCATION_OF_YOUR_KEYFILE LOCATION_OF_YOUR_ROOT_CERT PROTECTED_LOCATION LOCATION_2_ON_FILE_SYSTEM

if [[ -z "$PROTECTED_LOCATION" || -z "$LOCATION_2_ON_FILE_SYSTEM" ]]
then
    echo "Either the path for PROTECTED_LOCATION or LOCATION_2_ON_FILE_SYSTEM have not been set. Please check your values.yaml."
    exit 1
fi

if [[ -z "$LOCATION_OF_YOUR_CERTIFICATE" || -z "$LOCATION_OF_YOUR_KEYFILE" || -z "$LOCATION_OF_YOUR_ROOT_CERT" || $ENABLE_APACHE_SSL == false ]]
then 
    echo "One or more variables for default-ssl.conf have not been set. If you want Apache to handle SSL by itself, please check your config for LOCATION_OF_YOUR_CERTIFICATE, LOCATION_OF_YOUR_KEYFILE, LOCATION_OF_YOUR_ROOT_CERT, PROTECTED_LOCATION and LOCATION_2_ON_FILE_SYSTEM. If your k8s-ingress is taking care of that, you can safely ignore this message..."
else
    sed -i "s#WEBMASTER_MAIL#$WEBMASTER_MAIL#g" /etc/apache2/sites-available/default-ssl.conf
    sed -i "s#YOUR_SERVER_NAME#$YOUR_SERVER_NAME#g" /etc/apache2/sites-available/default-ssl.conf
    sed -i "s#LOCATION_OF_YOUR_CERTIFICATE#$LOCATION_OF_YOUR_CERTIFICATE#g" /etc/apache2/sites-available/default-ssl.conf
    sed -i "s#LOCATION_OF_YOUR_KEYFILE#$LOCATION_OF_YOUR_KEYFILE#g" /etc/apache2/sites-available/default-ssl.conf
    sed -i "s#LOCATION_OF_YOUR_ROOT_CERT#$LOCATION_OF_YOUR_ROOT_CERT#g" /etc/apache2/sites-available/default-ssl.conf
    sed -i "s#PROTECTED_LOCATION#$PROTECTED_LOCATION#g" /etc/apache2/sites-available/default-ssl.conf
    sed -i "s#LOCATION_2_ON_FILE_SYSTEM#$LOCATION_2_ON_FILE_SYSTEM#g" /etc/apache2/sites-available/default-ssl.conf
fi

# also changing the keywords in 000-default.conf if SSL/TLS is managed by a k8s ingress. Then, default-ssl.conf should not be enabled and neither should mod_ssl.
sed -i "s#WEBMASTER_MAIL#$WEBMASTER_MAIL#g" /etc/apache2/sites-available/000-default.conf
sed -i "s#YOUR_SERVER_NAME#$YOUR_SERVER_NAME#g" /etc/apache2/sites-available/000-default.conf
sed -i "s#PROTECTED_LOCATION#$PROTECTED_LOCATION#g" /etc/apache2/sites-available/000-default.conf
sed -i "s#LOCATION_2_ON_FILE_SYSTEM#$LOCATION_2_ON_FILE_SYSTEM#g" /etc/apache2/sites-available/000-default.conf
sed -i "s#ALLOWED_IP_RANGE#$ALLOWED_IP_RANGE#g" /etc/apache2/sites-available/000-default.conf


# changing keyword in auth-checker.lua
sed -i "s#LOCATION_2_ON_FILE_SYSTEM#$LOCATION_2_ON_FILE_SYSTEM#g" /var/run/apache2/auth_checker.lua
# activate sites, modules and configs & start apache2 daemon

if [ $ENABLE_APACHE_SSL == true ]
then
    a2enmod ssl
    a2ensite default-ssl.conf
fi

a2enconf openidc

source /etc/apache2/envvars

#create DavLock
touch $LOCATION_2_ON_FILE_SYSTEM/DavLock
chown www-data:www-data $LOCATION_2_ON_FILE_SYSTEM/DavLock

#create digest root dir
mkdir -p $LOCATION_2_ON_FILE_SYSTEM/hashes
chown www-data:www-data $LOCATION_2_ON_FILE_SYSTEM/hashes
# create directory in digest root dir such that it cannot be deleted.
mkdir -p $LOCATION_2_ON_FILE_SYSTEM/hashes/DO_NOT_DELETE
chmod go-rw $LOCATION_2_ON_FILE_SYSTEM/hashes/DO_NOT_DELETE

# execute the watcher service
bash /home/watcher.bash $PROTECTED_LOCATION ${LOCATION_2_ON_FILE_SYSTEM}hashes &> ~/watcher.log &

/usr/sbin/apache2 -f /etc/apache2/apache2.conf -D "FOREGROUND"
wait $!
