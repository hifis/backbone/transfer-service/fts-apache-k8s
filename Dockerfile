FROM ubuntu:20.04
LABEL maintainer="tim.wetzel@desy.de"

ENV DEBIAN_FRONTEND=noninteractive
# install fail2ban & apache packages
RUN apt-get update && \
    apt-get install -y fail2ban apache2 apache2-bin apache2-dev libapache2-mod-auth-openidc git zlib1g-dev libcap-dev inotify-tools

RUN apache2ctl stop
COPY envvars /etc/apache2/envvars
# create directories for modified modules, copy files & compile, install and activte modified modules

RUN cd /home && git clone https://github.com/wetzel-desy/mod_want_digest.git
RUN cd /home/mod_want_digest && \
    apxs -cia mod_want_digest.c

RUN cd /home && curl -O http://mpm-itk.sesse.net/mpm-itk-2.4.7-04.tar.gz && tar xzf mpm-itk-2.4.7-04.tar.gz
COPY mpm_itk.c /home/mpm-itk-2.4.7-04/mpm_itk.c
RUN cd /home/mpm-itk-2.4.7-04 && \
    ls -al && ./configure && make && make install

# copy config and load file for mpm_itk
COPY mpm_itk.load /etc/apache2/mods-available/mpm_itk.load
COPY mpm_itk.conf /etc/apache2/mods-available/mpm_itk.conf
# disable mpm_event because of conflict with mpm_itk
RUN a2dismod mpm_event
# activate apache modules needed
RUN a2enmod rewrite dav dav_fs dav_lock lua mpm_itk mpm_prefork

# copy site-configs
COPY ports.conf /etc/apache2/ports.conf
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
COPY default-ssl.conf /etc/apache2/sites-available/default-ssl.conf

# copying lua script for local user mapping.
COPY auth_checker.lua /var/run/apache2/auth_checker.lua
#COPY userfile.csv /var/run/apache2/userfile.csv

# copy template of openidc.conf, the values will be replaced by start.sh during container startup
COPY openidc.conf /etc/apache2/conf-available/openidc.conf

COPY watcher.bash /home/watcher.bash
RUN chmod +x /home/watcher.bash
COPY dir_watcher.py /home/dir_watcher.py
RUN chmod +x /home/dir_watcher.py
# copy startup script
COPY start.sh /home/start.sh
RUN chmod +x /home/start.sh

ENTRYPOINT ["bash", "/home/start.sh"]
#CMD ["/usr/sbin/apache2", "-f", "/etc/apache2/apache.conf", "-D", "FOREGROUND"]
